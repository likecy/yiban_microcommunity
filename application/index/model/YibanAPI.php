<?php
/**
 * Created by PhpStorm.
 * User: chenyun
 * Date: 2019-02-18
 * Time: 11:09
 */

namespace app\index\model;


use likecy\yiban\YBOpenApi;
use think\Model;

class YibanAPI extends  Model
{

    public static function LoginSetToken(){
      
       
        $yibanAPi= YBOpenApi::getInstance()->init(config('yibanAppID'),config('yibanAppSecret'),config('yibanCallBackUrl'));
        $iapp = $yibanAPi->getIApp();
        try{
            $info = $iapp->perform();
        }catch (YBException $ex){
            echo $ex->getMessage();
        }
        $token = $info['visit_oauth']['access_token'];//轻应用获取的token
        if ( $token) {
            session('token', $token);
            // $yibanAPi->bind($token);
            return true;
        }
        else{
            return false;
        }
        //  return $yibanAPi;
    }



public static function schoolBasemsg(){
  if(!isset($_GET['verify_request'])){//未授权跳转到页面;
            $this->redirect(\config('yibanCallBackUrl'));
     }
   if(self::LoginSetToken()){
    $yibanAPi = self::ApiInit();
    $basemsg = $yibanAPi->request('user/verify_me');
    $infomsg = $yibanAPi->request('user/real_me');
    $basemsg['info']['yb_userhead'] = $infomsg['info']['yb_userhead'];
    $basemsg['info']['yb_usernick'] = $infomsg['info']['yb_usernick'];
    $basemsg['info']['yb_money'] = $infomsg['info']['yb_money'];
    $basemsg['info']['yb_sex'] = $infomsg['info']['yb_sex'];
    $basemsg['info']['yb_birthday'] = $infomsg['info']['yb_birthday'];
    $basemsg['info']['yb_exp'] = $infomsg['info']['yb_exp'];
    $basemsg['info']['yb_identity'] = $infomsg['info']['yb_identity'];
    return $basemsg['info'];
   }
}



    public static function ApiInit(){
        $yibanAPi= YBOpenApi::getInstance()->init(config('yibanAppID'),config('yibanAppSecret'),config('yibanCallBackUrl'));
        $yibanAPi->bind(session('token'));
        return $yibanAPi;
    }


}