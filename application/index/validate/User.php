<?php
namespace app\index\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'username'         => 'require|unique:user',
        'password'         => 'confirm:confirm_password|',
        'confirm_password' => 'confirm:password',
        'usermail'            => 'unique:user',
    ];

    protected $message = [
        'username.require'         => '请输入用户名',
    		'username.min'         => '用户名至少6位',
        'username.unique'          => '用户名已存在',
    		'usermail.unique'          => '邮箱已存在',
        'password.confirm'         => '两次输入密码不一致',
    		'password.length'         => '密码在6位到16位之间',
        'confirm_password.confirm' => '两次输入密码不一致',
        'usermail.email'              => '邮箱格式错误',
    ];
    
    protected $scene = [
    		'passwordedit'  =>  ['password','confirm_password'],
    ];
    
}